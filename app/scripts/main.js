'use strict';
(function(){

    console.log('Tsukkomi loading');

    var color = $('#color');
    var comment = $('#comment');
    var button = $('#submit');
    var buttonLabel = $('.ladda-label');

    var origButtonColor = button.attr('data-color');
    var origButtonLabel = buttonLabel.text();

    // ladda
    var buttonSpinner = button.ladda();

    // initialize colorpicker
    var cookieColor = Cookies.get('color');
    if (cookieColor) {
        color.val(cookieColor);
    }

    color.colorpicker();

    // form submit
    $('#main').submit(function(e){
        e.preventDefault();
        // validation
        var textComment = comment.val().trim(),
            textColor = color.val().trim();
        if (!textComment) {
            $('.col-md-10').addClass('has-error');
            comment.attr('placeholder', '至少说点什么啦…');
            return;
        }
        $('.col-md-10').removeClass('has-error');
        comment.attr('disabled', true);
        button.attr('disabled', true);
        buttonSpinner.ladda('start');
        $.ajax({
            type: 'POST',
            data: {
                comment: textComment,
                color: textColor
            },
            url: '/post',
            dataType: 'json'
        }).done(function(data){
            var buttonColor, labelText;
            comment.attr('disabled', false);
            button.attr('disabled', false);
            switch (data.status) {
                case 0:
                    buttonColor = 'green';
                    labelText = '发送成功！';
                    comment.val('');
                    break;
                case 1:
                    buttonColor = 'purple';
                    labelText = '手速太快，请放慢一点…';
                    break;
                case 2:
                    buttonColor = 'red';
                    labelText = '不要说奇怪的话→_→';
                    comment.val('');
                    break;
                case 5:
                    buttonColor = 'cyan';
                    labelText = '太长了！';
                    break;
            }
            button.attr('data-color', buttonColor);
            buttonLabel.text(labelText);
            buttonSpinner.ladda('stop');
            setTimeout(function(){
                button.attr('data-color', origButtonColor);
                buttonLabel.text(origButtonLabel);
            }, 2000);
        }).fail(function(xhr){
            var buttonColor, labelText;
            comment.attr('disabled', false);
            button.attr('disabled', false);
            switch(xhr.statusCode()) {
                case 413:
                    buttonColor = 'cyan';
                    labelText = '太长了！';
                    break;
                default:
                    buttonColor = 'red';
                    labelText = '请稍后重试';
            }
            button.attr('data-color', buttonColor);
            buttonLabel.text(labelText);
            buttonSpinner.ladda('stop');
            setTimeout(function(){
                button.attr('data-color', origButtonColor);
                buttonLabel.text(origButtonLabel);
            }, 2000);
        });
    });

    console.log('Tsukommi loaded');
})();
